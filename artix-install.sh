#!/bin/bash
partitioning(){
	lsblk
	echo "set install drive"
	read disk
	clear
	echo "fill disk with zeros?
	1) yes
	2) no"
	read answr
	[ "$answr" == "1" ] && dd if=/dev/zero of=$disk status=progress
	cfdisk $disk
}
formatting(){
	sleep 5s
	lsblk
	echo "set boot partition"
	read bootpart
	clear
	lsblk
	echo "set root parttion"
	read rootpart
	clear
	echo "set up swap?
	1) yes
	2) no"
	read answr
	[ "$answr" == "1" ] && set-up-swap 
	mkfs.vfat $bootpart
	mkfs.ext4 $rootpart 
}
set-up-swap(){
	clear
	lsblk
	echo "set swap partition"
	read swappart
	mkswap $swappart
}
dir(){
	mount $rootpart /mnt
	mkdir /mnt/boot
	mount $bootpart /mnt/boot
}
base-system(){
	ping -c3 google.com || (echo "please check your internet connection and try again next time" ; exit)
	basestrap /mnt base openrc connman-openrc grub connman-gtk elogind-openrc linux linux-firmware dhcpcd nano gvim mc exfat-utils btrfs-progs ntfs-3g htop dosfstools efibootmgr
}
my-base-devel(){
	basestrap /mnt fakeroot autoconf automake bison flex gcc make patch pkgconf wget opendoas 
	echo "permit persist leonidas as root" >> /mnt/etc/doas.conf 
}
fstab(){
	uuid=$(lsblk -fd $bootpart -o UUID | sed s/"UUID"/""/g | sed '/^$/d;s/[[:blank:]]//g')
	fstype=$(lsblk -f $bootpart -o FSTYPE | sed s/"FSTYPE"/""/g | sed '/^$/d;s/[[:blank:]]//g')
	echo "UUID=$uuid /boot $fstype defaults 0 2" >> /mnt/etc/fstab
	[ "$swappart" == "" ] || fstab-swap
}
fstab-swap(){
	uuid=$(lsblk -fd $swappart -o UUID | sed s/"UUID"/""/g | sed '/^$/d;s/[[:blank:]]//g')
	echo "UUID=$uuid none swap defaults 0 0" >> /mnt/etc/fstab
}
clear
partitioning
clear
formatting
clear
dir
clear
base-system
clear
my-base-devel
clear
mv system.sh /mnt
artix-chroot /mnt bash system.sh
fstab
