#!/bin/bash
locale(){
	echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
	echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen
	locale-gen
}
time-one(){
	ln -sf /usr/share/zoneinfo/Asia/Tomsk /etc/localtime
	hwclock --systohc
}
grub(){
	grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
	grub-mkconfig -o /boot/grub/grub.cfg
}
user-managment(){
	clear
	echo "set password for root"
	passwd
	useradd -m leonidas
	clear
	echo "set password for user"
	passwd leonidas
	echo "hostname='PC'" >> /etc/conf.d/hostname
}
basic-desktop(){
	pacman -S plasma konsole dolphin kwrite spectacle plasma-wayland-session gwenview ark p7zip unrar unarchiver lzop lrzip okular filelight networkmanager flatpak sddm-openrc
	rc-update add sddm
}
locale
time-one
grub
user-managment
basic-desktop
rm system.sh